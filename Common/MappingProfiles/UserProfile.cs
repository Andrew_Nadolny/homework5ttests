﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Common.DTO;
using Common.Models;

namespace Common.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDTO, User> ();
            CreateMap<User, UserDTO>();

        }
    }
}

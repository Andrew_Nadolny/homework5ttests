﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Common.DTO;
using Common.Models;

namespace Common.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();

        }
    }
}

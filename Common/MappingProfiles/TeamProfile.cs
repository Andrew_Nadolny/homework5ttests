﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Common.DTO;
using Common.Models;

namespace Common.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
            CreateMap<Team, TeamDTO>();

        }
    }
}

﻿using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.Queries;
using Common.Models;
using Common.DTO;

namespace HomeWork3ProjectStructure.Processors
{
    public class QuerieProcessor : IQuerieProcessor
    {
        public QuerieHandler _querieHandler { get; set; }
        public QuerieProcessor(QuerieHandler querieHandler)
        {
            _querieHandler = querieHandler;
        }

        public List<ProjectDTO> Processed(GetProjectsQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }

        public List<TaskDTO> Processed(GetTasksQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public List<TeamDTO> Processed(GetTeamsQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public List<UserDTO> Processed(GetUsersQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public ProjectDTO Processed(GetProjectByIdQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public TeamDTO Processed(GetTeamByIdQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public TaskDTO Processed(GetTaskByIdQuerie querie)
        {
            return  _querieHandler.Handle(querie);
        }
        public UserDTO Processed(GetUserByIdQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public Dictionary<int, int> Processed(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<(int Id, string Name, List<UserDTO> Teammates)> Processed(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<(UserDTO User, List<TaskDTO> Tasks)> Processed(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)> Processed(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<TaskDTO> Processed(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<TaskDTO> Processed(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)> Processed(GetUserWithLastProjectAndETCQuerie querie)
        {
            return _querieHandler.Handle(querie);
        }

        public List<TaskDTO> Processed(GetUserUnfinishedTasks querie)
        {
            return _querieHandler.Handle(querie);
        }
    }
}

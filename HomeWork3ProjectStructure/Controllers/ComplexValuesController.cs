﻿using AutoMapper;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using HomeWork3ProjectStructure.DAL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexValuesController : ControllerBase
    {

        private readonly QuerieProcessor _querieProcessor;

        public ComplexValuesController(ProjectsDbContext _context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(_context, mapper));

        }

        [Route("GetCountTasksInProjectByAuthorId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetCountTasksInProjectByAuthorId(int id)
        {
            return JsonConvert.SerializeObject( _querieProcessor.Processed(new GetCountTasksInProjectByAuthorIdQuerie() { Id = id }));
        }

        [Route("GetListOfTeamsWithTeamatesOlderThen10")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetListOfTeamsWithTeamatesOlderThen10()
        {
            return JsonConvert.SerializeObject(_querieProcessor.Processed(new GetListOfTeamsWithTeamatesOlderThen10Querie()));
        }

        [Route("GetListOfUsersAscendingWhitTasksDescending")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetListOfUsersAscendingWhitTasksDescending()
        {
            return JsonConvert.SerializeObject(_querieProcessor.Processed(new GetListOfUsersAscendingWhitTasksDescendingQuerie()));
        }

        [Route("GetProjectWithTaskWithLongestDescriptionsAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetProjectWithTaskWithLongestDescriptionsAndETC()
        {
            return JsonConvert.SerializeObject( _querieProcessor.Processed(new GetProjectWithTaskWithLongestDescriptionsAndETCQuerie()));
        }

        [Route("GetUserWithLastProjectAndETC")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetUserWithLastProjectAndETC()
        {
            return JsonConvert.SerializeObject( _querieProcessor.Processed(new GetUserWithLastProjectAndETCQuerie()));
        }


        [Route("GetUserTasksWithShortNameByUserId")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetUserTasksWithShortNameByUserId(int id)
        {
            return JsonConvert.SerializeObject(_querieProcessor.Processed(new GetUserTasksWithShortNameByUserIdQuerie() { Id = id }));
        }

        [Route("GetUserTasksFinishedInCurrentYear")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public string GetUserTasksFinishedInCurrentYear(int id)
        {
            return JsonConvert.SerializeObject(_querieProcessor.Processed(new GetUserTasksFinishedInCurrentYearQuerie() { Id = id }));
        }

        [Route("GetUserUnfinishedTasks")]
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public IActionResult GetUserUnfinishedTasks(int id)
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(_querieProcessor.Processed(new GetUserUnfinishedTasks() { Id = id })));
            }
            catch(ArgumentException)
            {
                return BadRequest();
            }
            catch (AggregateException)
            {
                return NoContent();
            }
        }
    }
}

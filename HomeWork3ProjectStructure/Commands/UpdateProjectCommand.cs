﻿using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.Models;
using Common.DTO;

namespace HomeWork3ProjectStructure.Commands
{
    public class UpdateProjectCommand : ICommand<ProjectDTO>
    {
        public ProjectDTO project { get; set; }
    }
}

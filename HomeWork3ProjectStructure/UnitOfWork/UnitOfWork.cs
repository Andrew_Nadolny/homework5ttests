﻿using HomeWork3ProjectStructure.Repositores;
using Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.DAL;
using Common.Models;


namespace HomeWork3ProjectStructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectsDbContext _context;
        public UnitOfWork(ProjectsDbContext context)
        {
            _context = context;
        }
        public IRepository<TEntity> Set<TEntity>() where TEntity : Entity
        {
            return new Repository<TEntity>(_context);
        }

        public int SaveChanges()
        {
           return  _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}

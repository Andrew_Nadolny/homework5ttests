﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeWork3ProjectStructure.Migrations
{
    public partial class ChangeFieldsInTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TaskState",
                table: "Tasks",
                newName: "State");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "State",
                table: "Tasks",
                newName: "TaskState");
        }
    }
}

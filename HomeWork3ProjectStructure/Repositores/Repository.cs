﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HomeWork3ProjectStructure.DAL;
using Common.Models;
using Microsoft.EntityFrameworkCore;

namespace HomeWork3ProjectStructure.Repositores
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        public ProjectsDbContext _context;

        public Repository(ProjectsDbContext context)
        {
            _context = context;
            context.ChangeTracker.Clear();
        }
        public List<TEntity> Get()
        {
            return _context.Set<TEntity>().ToList();
        }

        public TEntity Get(int id)
        {
            return _context.Set<TEntity>().SingleOrDefault(x => x.Id == id);
        }

        public TEntity Create(TEntity entity)
        {
            return _context.Set<TEntity>().Add(entity).Entity;
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            return (await _context.Set<TEntity>().AddAsync(entity)).Entity;
        }

        public bool Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            _context.Entry(entity).State = EntityState.Modified;
            return !_context.Set<TEntity>().Any(x => x == entity);
        }

        public bool Delete(int id)
        {
            _context.Set<TEntity>().Remove(_context.Set<TEntity>().SingleOrDefault(x=> x.Id == id));
            return !_context.Set<TEntity>().Any(x => x.Id == id);
        }


        public TEntity Update(TEntity entity)
        {
           return _context.Set<TEntity>().Update(entity).Entity;
        }

    }
}

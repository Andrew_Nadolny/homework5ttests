﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Interfaces
{
    interface IQuerieHandler<TQuerie, TResult> where TQuerie : IQuerie<TResult>
    {
        public TResult Handle(TQuerie querie);
    }
}

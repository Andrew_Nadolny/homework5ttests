﻿using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Reguests
{
    class TeamRequest
    {
        public string ApiUrl = "https://localhost:44309";
        public async Task<List<Team>> GetTeamsAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Team> teams = new List<Team>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Teams");
                if (response.IsSuccessStatusCode)
                {
                    teams = JsonConvert.DeserializeObject<List<Team>>(await response.Content.ReadAsStringAsync());
                }
                return teams;
            }
        }

        public async Task<Team> GetTeamAsync(int teamId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Team teams = new Team();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Teams/{0}", teamId));
                if (response.IsSuccessStatusCode)
                {
                    teams = JsonConvert.DeserializeObject<Team>(await response.Content.ReadAsStringAsync());
                }
                return teams;
            }
        }
    }
}

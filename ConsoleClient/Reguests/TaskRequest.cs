﻿using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ConsoleClient.Reguests
{
    public class TaskRequest
    {
        private string ApiUrl = "https://localhost:44309";
        public async System.Threading.Tasks.Task<List<Task>> GetTasksAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                List<Task> tasks = new List<Task>();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync("/api/Tasks");
                if (response.IsSuccessStatusCode)
                {
                    tasks = JsonConvert.DeserializeObject<List<Task>>(await response.Content.ReadAsStringAsync());
                }
                return tasks;
            }
        }

        public async System.Threading.Tasks.Task<Task> GetTaskAsync(int taskId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Task tast = new Task();
                httpClient.BaseAddress = new Uri(ApiUrl);
                HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/Tasks/{0}", taskId));
                if (response.IsSuccessStatusCode)
                {
                    tast = JsonConvert.DeserializeObject<Task>(await response.Content.ReadAsStringAsync());
                }
                return tast;
            }
        }
    }
}
